#!/usr/bin/python3
# Just a fun little way to turn a SenseHat LED matrix into a waterfall display
# or progress counter.
# Good way to teach kids about nested loops, Cartesian coordinates, and matrices.
# With shorter sleep times, modify the color of each 'pixel' along the RGB spectrum for more
# 'wow'.

from sense_emu import SenseHat # replace sense_emu with sense_hat for the real-deal.
import time
sense = SenseHat()
sense.clear()
r = (255,0,0)
g = (0,255,0)
b = (0,0,255)
while True:
	for x in range(8):
		for y in range(8):
			sense.set_pixel(x,y,r)
			time.sleep(0.01)
			sense.set_pixel(x,y,g)
			time.sleep(0.01)
			sense.set_pixel(x,y,b)
			time.sleep(0.01)
			if (x == y):
				sense.set_pixel(x,y,g)
				time.sleep(0.02)
				sense.set_pixel(x,y,r)
	sense.clear()